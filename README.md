Descriere mod de functionare:

Standardul de compresie a unei imagini, JPEG, are la baza 4 pasi: preprocesare, transformare, cuantificare si codificare. JPEG utilizeaza o versiune mai avansata a codificarii Huffman pentru a realiza ultimul pas al algoritmului.
Pentru o imagine RGB se realizeaza prima oara conversia canalelor R, G, B in spatiul YCbCr, lucrandu-se in continuare pe canalele Y, Cb si Cr. Dupa aceea, imaginea se imparte in blocuri de 8x8 pixeli.
Trecerea de la RGB la YCbCr s-a realizat in kernel-ul „color_space_transform”
Fiecare kernel se ocupa de transformarea unui singur pixel pentru toate componentele sale R, G, B in componentele Y, Cb si Cr.
Este folosit pentru transformare tabelul „color_conversion_table” scris de pe host cu proprietatea CL_MEM_READ_ONLY.
Dupa transformarea din RGB in spatiul YCbCr se realizeaza centrarea pe 0 a valorilor componentelor Y, Cb si Cr, realizat de kernel-ul „downsample_full”:
In functie de marimea imaginii se calculeaza numarul de blocuri de 8x8 necesare incadrarii acesteia, in fiecare dintre cele 2 directii x si y. De asemenea, se vor calcula numarul de superblocuri care contin 2x2 blocuri de 8x8, deci care au marime 16x16. Numarul total de work-items este dat de numarul de superblocuri inmultit cu cele 256 de componente din fiecare superbloc. Y_block_buffer reprezinta un vector global care retine componenta y. Aceasta operatie se va realiza separate pe y si pe CbCr (acestea sunt aspect care tin de algoritmul JPEG).
Y_block_buffer are atributul CL_MEM_READ_WRITE si este global, de marimea imaginii.

Centrarea se realizeaza pornind de la index-ul global „gx”, calculandu-se pe rand pozitia superblocului pe directia x si y, pozitia blocului pe directia x si y si pozitia pe x si y in cadrul blocului. Folosind aceste valori se va calcula pozitia absoluta in imagine (matrice bi-dimensionala) a componentei curente. Pozitia din memoria continua va fi luata ca „x + (y*width) * 3”.

JPEG utilizeaza Transformata Cosinus Discreta (DCT) pentru a transforma imaginea. C = U*B*UT, unde B – un bloc 8x8 din imaginea preprocesata, iar U este o matrice 8x8 speciala. DCT tinde sa impinga valorile mari (intensitatile mari) din blocul 8x8 in partea de stanga-sus a matricei C, astfel restul valorilor din C ramanand relative mici. DCT-ul se aplica pe fiecare bloc in parte.

 In cadrul proiectului realizat in OpenCL se va utiliza algoritmul DCT cu 11 inmultiri. Algoritmul este aplicat separat pe randurile si coloanele blocurilor 8x8, mai exact se vor procesa toate randurile, urmate de toate coloanele. La iesire, se poate observa ca parcursul pentru bitii de pe pozitiile pare este diferit de parcursul pentru bitii de pe pozitiile impare. De asemenea, pixelii 0 si 4 din input sunt tratati diferit de pixelii de pe pozitiile 2 si 6. Fluxul este de la stanga la dreapta. Acest algoritm are ca input 8 biti si ca output 8 biti transformati.

La cuantificare, Scopul este ca numerele apropriate de 0 sa fie convertite la 0, iar celelalte elemente sa fie micsorate astfel incat valorile sa fie mai apropiate de 0.  De asemenea se doreste obtinerea unor valori intregi, intrucat DCT returneaza valori reale.

Ultimul pas reprezinta aplicarea Huffman ce realizeaza codificarea imaginii transformata si cuantificata. Imaginea procesata de mine este de dimensiune 160 x 240 si avea nevoie de 160*240*8 = 307,200 biti pentru a fi stocata, iar in urma compresiei JPEG este nevoie de 85,143 biti, conversia fiind de 2,217 bpp. Spatiul de stocare necesar este redus cu 70%.

Implementare:

Implementarea a fost realizata in limbajul C++ folosind paralelizarea pe GPU in cadrul OpenCL; Exista 6 kernele folosite pentru realizarea primelor etape ale algoritmului/Pe host s-a realizat codificarea entropica. Kernel-ul cel mai important este „dct_quant” ce se ocupa cu etapele 2 si 3, de transformare DCT si cuantificare, acestea fiind cele mai importante, kernel-ul fiind cel mai complex. In constructor au fost definite mai multe structuri de date ajutatoare declarate ca buffere. Acestea vor fi folosite mai departe in kernele. Au fost definite cu read_only pentru ca nu vor fi modificate de gpu. Tabelele „MULTIPLIER”, „SIGN” si „INDICES” sunt folosite pentru a face diferenta intre pozitia din randul/coloana curenta din blocul in care ne aflam. Data fiind natura algoritmului cu 11 inmultiri si implementarea realizata in OpenCL per kernel, ce necesita scrierea unei singure variante care sa functioneze pentru fiecare pixel, solutia utilizarii acestor tabele face operatiile dependente de pozitia din structura de date. Componenta y a imaginii este definita in OpenCL ca „y_block_buffer. Aceasta componenta este definita in cadrul unui buffer definit cu „CL_MEM_READ_WRITE” deoarece modificarile din cadrul kernel-ului se vor face direct asupra acestui bloc, asadar kernel-ul trebuie sa aiba atat drepturi de citire, „READ”, cat si de scriere, „WRITE”.

In cadrul acestui kernelului DCT_QUANT s-a utilizat pentru sincronizare o bariera, apelata prin functia „barrier”. Parametrul „CLK_LOCAL_MEM_FENCE” indica barierei faptul ca operatiile asupra memoriei locale trebuie facute intr-o ordine corecta, functia executandu-se pentru fiecare work-item dintr-un work-group ce executa acest kernel. In memoria locala este creata si o structura de 64 short-uri, numita „lblock”, cu ajutorul carora se vor scrie valorile fiecarui work-item in memoria locala prin secventa de cod „lblock[lx] = block[gx]”. Niciun work-item nu poate continua executia kernel-ului pana cand nu au trecut toate celelalte work-iteme prin executia barierei.
De asemenea, se calculeaza cateva valori private (row, row_offset si column) pentru a putea sa incadram in cadrul unui bloc de 8x8 componenta curenta lx din cadrul memoriei continue locale.

Variabilele private „ioffset”, „moffset”, „soffset” si „doffset” sunt folosite pentru a extrage din tabelele prezentate anterior datele necesare pentru a aplica operatiile corecte pentru pixelul de la pozitia curenta. Se poate observa ca pentru procesarea pe randuri a fost folosita variabila „column” pentru a discerne unde ne aflam in rand.
Pointerul dataptr primeste adresa de memorie din lblock corespunzatoare coloanei. Astfel, se asigura faptul ca multiplii reprezinta datele din coloana curenta.
De exemplu, daca am fi aratat catre inceputul lui lblock, column ar fi putut fi diferit de 0, de exemplu 3, iar adresele din lblock corespunzatoare celorlalte valori din coloana ar fi
devenit 11, 19, 27 etc, care nu sunt multiplii de 8.
Se poate observa ca pentru procesarea pe randuri a fost folosita variabila row pentru a discerne unde ne aflam in coloana.

Surse:
-	„Practical Fast 1-D DCT ALGORITHMS WITH 11 MULTIPLICATIONS” – Loeffler C., Ligtenberg A., Moschytz G.S. (1999) -> https://ieeexplore.ieee.org/document/266596 -> daca vreti sa o cititi -> ( https://sci-hub.hkvisa.net/10.1109/ICASSP.1989.266596 )
-	https://blog.katastros.com/a?ID=00600-2445d11f-d040-449b-b393-b2f9ab10a047
-	http://pi.math.cornell.edu/~web6140/TopTenAlgorithms/JPEG.html
-	https://www.youtube.com/watch?v=aFbGqXFT0Nw









